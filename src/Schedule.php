<?php

// Chrome - Developer Tools - Network
// 在 http://www.wanplus.com/lol/schedule
// 点击上一周赛程按钮，势必向 server 发 request 索取信息，活捉
// http://www.wanplus.com/ajax/schedule/list
// 
// 就默认的陈列方式就好，不要点 "view source"
// 复制 Response Headers 下的内容
// Postman 的 Header 点 Bulk Edit
// 粘贴
// 再点 Key-Value Edit 即可
// 
// 复制 Form Data 下的内容
// Postman 的 Body 的 form-data 点 Bulk Edit
// 粘贴
// 再点 Key-Value Edit 即可


// 1. 抓取

// 2. 写入 file(db)

namespace Esports;

class Schedule 
{
    /**
     * @param string $game 2:lol
     * @param string $date timestamp
     */
    public function get($game = '2', $eids = '', $date) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://www.wanplus.com/ajax/schedule/list",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"_gtk\"\r\n\r\n1341990775\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"game\"\r\n\r\n".$game."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"time\"\r\n\r\n".$date."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"eids\"\r\n\r\n".$eids."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json, text/javascript, */*; q=0.01",
            "Accept-Encoding: gzip, deflate",
            "Accept-Language: en,zh-CN;q=0.9,zh;q=0.8",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Content-Length: 44",
            "Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
            "Cookie: wanplus_token=ed9e26ccec8422bfaa32dcc6e816866e; wanplus_storage=lf4m67eka3o; wanplus_sid=6c4cafd32ee7c1d60155d74c6e851e02; wanplus_csrf=_csrf_tk_1258104695; isShown=1; wp_pvid=4550152217; wp_info=ssid=s5326644917; Hm_lvt_f69cb5ec253c6012b2aa449fb925c1c2=1527042310; gameType=2; Hm_lpvt_f69cb5ec253c6012b2aa449fb925c1c2=1527065959",
            "Host: www.wanplus.com",
            "Origin: http://www.wanplus.com",
            "Postman-Token: ef228636-ac2f-4258-9c7f-5186bcd040f0",
            "Referer: http://www.wanplus.com/lol/schedule",
            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36",
            "X-CSRF-Token: 1341990775",
            "X-Requested-With: XMLHttpRequest",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          //echo "cURL Error #:" . $err;
          return "cURL Error #:" . $err;
        } else {
          //echo $response;
          return $response;
        }
    }
}
